from random import randrange


def main():
    """Very important docstring"""

    value = randrange(100)
    print(f'The random value is {value}')

    if value % 2 == 0:
        print('Behold! An even number.')
    else:
        print('Hmm, an odd number.')

    if value > 50:
        print('One ring to rule them all')
    elif value < 10:
        print('Time for second breakfast!')
    else:
        print('Fly, you fools!')


if __name__ == '__main__':
    main()
